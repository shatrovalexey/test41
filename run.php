<?php
	ini_set( 'display_errors' , E_ALL ) ;

	function hasClass( $className ) {
		return sprintf( '
contains(
	concat(
		" " ,
		normalize-space( @class ) ,
		" "
	) ,
	" %s "
)
		' , htmlspecialchars( $className , ENT_XML1 , CHARSET ) ) ;
	}

	define( 'URL' , 'https://twitter.com/bbcrussian' ) ;

	$domh = new DOMDocument( ) ;
	@$domh->loadHTMLFile( URL ) ;
	$xpathh = new DOMXPath( $domh ) ;
	$charset = 'utf8-mb4' ;

	foreach ( $xpathh->query( '
//meta[@charset]
/@charset
	' ) as $node_charset ) {
		$charset = $node_charset->value ;
	}

	define( 'CHARSET' , $charset ) ;

	$initial_data = $domh->getElementById( 'init-data' )->getAttribute( 'value' ) ;
	$initial_data = json_decode( $initial_data ) ;

	$stream_url = parse_url( URL ) ;
	$stream_url[ 'path' ] = $initial_data->timeline_url ;
	$stream_url = sprintf( '%s://%s%s' , $stream_url[ 'scheme' ] , $stream_url[ 'host' ] , $stream_url[ 'path' ] ) ;

	$max_position = -1 ;

	$dbh = new PDO( 'mysql:host=localhost;dbname=test41' , 'root' , 'f2ox9erm' ) ;
	$dbh->setAttribute( PDO::ATTR_ERRMODE , PDO::ERRMODE_EXCEPTION ) ;
	$sth_news_ins = $dbh->prepare( '
INSERT IGNORE INTO
	`news`
SET
	`id` := :id_news ,
	`created` := from_unixtime( :created ) ,
	`text` := :text ;
	' ) ;

	$sth_news_hash_ins = $dbh->prepare( '
INSERT IGNORE INTO
	`news_hash`
SET
	`id_news` := :id_news ,
	`value` := :value ;
	' ) ;

	while ( true ) {
		if ( empty( $max_position ) ) {
			continue ;
		}
		if ( $max_position == -1 ) {
			$url = $stream_url ;
		}
		if ( is_numeric( $max_position ) ) {
			$url = sprintf(
				'%s?include_available_features=1&include_entities=1&' .
				'max_position=%s&oldest_unread_id=0&reset_error_state=false' ,
				$stream_url , urlencode( $max_position )
			) ;
		} else {
			continue ;
		}

		$data = json_decode( file_get_contents( $url ) ) ;

		if ( empty( $data ) ) {
			break ;
		}

		$domh = new DOMDocument( '1.0' , CHARSET ) ;
		@$domh->loadHTML( '<?xml version="1.0" encoding="' . CHARSET . '"?>' . $data->items_html ) ;
		$xpathh = new DOMXPath( $domh ) ;

		$max_position = null ;

		foreach ( $xpathh->query( '
//*[ ' . hasClass( 'stream-item' ) . ' ]
		' ) as $node_stream_item ) {

			$item = array(
				'text' => array( ) ,
				'hash' => array( ) ,
				'time' => null
			) ;

			foreach ( $xpathh->query( '
*/*[ ' . hasClass( 'content' ) . ' ]
			' , $node_stream_item ) as $node_content ) {
				foreach ( $xpathh->query( '
*[ ' . hasClass( 'js-tweet-text-container' ) . ' ]
/*[ ' . hasClass( 'js-tweet-text' ) . ' ]
				' , $node_content ) as $node_text_container ) {
					foreach ( $xpathh->query( '
a
					' , $node_text_container ) as $node_remove ) {
						if ( ! $node_remove->getAttribute( 'title' ) ) {
							continue 3 ;
						}

						$node_text_container->removeChild( $node_remove ) ;
					}

					foreach ( $xpathh->query( '
*[ ' . hasClass( 'twitter-hashtag' ) . ' ]
//b
/text( )
					' , $node_text_container ) as $node_hash ) {
						$item[ 'hash' ][] = $node_hash->textContent ;
					}

					$item[ 'text' ][] = $node_text_container->textContent ;
				}

				if ( empty( $item[ 'text' ] ) ) {
					continue 3 ;
				}

				foreach ( $xpathh->query( '
*[ ' . hasClass( 'stream-item-header' ) . ' ]
//*[ ' . hasClass( 'time' ) . ' ]
//*[ ' . hasClass( 'tweet-timestamp' ) . ' ]
//*[ ' . hasClass( '_timestamp' ) . ' ]
/@data-time
				' , $node_content ) as $node_datetime ) {
					$item[ 'time' ] = $node_datetime->value ;
				}

				if ( empty( $item[ 'time' ] ) ) {
					continue 3 ;
				}

				$item[ 'text' ] = implode( PHP_EOL , $item[ 'text' ] ) ;

				break ;
			}

			if ( empty( $item[ 'text' ] ) ) {
				continue ;
			}

			$id_news = $node_stream_item->getAttribute( 'data-item-id' ) ;

			if ( ! $sth_news_ins->execute( array(
				':id_news' => $id_news ,
				':text' => $item[ 'text' ] ,
				':created' => $item[ 'time' ]
			) ) ) {
				continue ;
			}

			foreach ( $item[ 'hash' ] as $hash ) {
				$sth_news_hash_ins->execute( array(
					':id_news' => $id_news ,
					':value' => $hash
				) ) ;
			}

			$max_position = $id_news ;
		}

		sleep( 10 ) ;
	}