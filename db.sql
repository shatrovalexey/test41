CREATE TABLE `news` (
  `id` bigint(22) unsigned NOT NULL COMMENT 'идентификатор twitter и локальный идентификатор',
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'дата\\время создания локальной записи в БД',
  `created` datetime NOT NULL COMMENT 'дата\\время создания записи в twitter',
  `text` longtext COMMENT 'текст новости',
  PRIMARY KEY (`id`),
  KEY `idx_created` (`created`,`datetime`),
  FULLTEXT KEY `ft_text` (`text`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='новость';

CREATE TABLE `news_hash` (
  `id_news` bigint(22) unsigned NOT NULL COMMENT 'идентификатор новости',
  `value` varchar(45) NOT NULL COMMENT 'значение',
  PRIMARY KEY (`id_news`,`value`),
  UNIQUE KEY `idx_value_id_news` (`value`,`id_news`) USING BTREE,
  CONSTRAINT `fk_news_hash_news` FOREIGN KEY (`id_news`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='хэш-тег новости';
